public class Problem8While {
    public static void main(String[] args) {
        int i = 1;
        while (i<=5) { 
            int j =1; // j อยู่ตรงนี้เพราะ j ใต้ while
            while (j<=5) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }

    }
}
