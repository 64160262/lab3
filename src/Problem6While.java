public class Problem6While {
    public static void main(String[] args) {
        int i = 0;
        while (i<5) { 
            int j =0; // j อยู่ตรงนี้เพราะ j ใต้ while
            while (j<5) {
                System.out.print("*");
                j++;
            }
            System.out.println();
            i++;
        }

    }
}
